"""
Guillem Teixido
16/12/22

User inputs two numbers
Print True or False whether the first number is greater than the second or not
"""
first_num = float(input("First number --> "))
second_num = float(input("Second number --> "))
if first_num > second_num: print(True)
else: print(False)
