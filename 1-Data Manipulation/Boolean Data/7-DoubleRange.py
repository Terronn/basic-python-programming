"""
Guillem Teixido
16/12/22

User inputs 5 numbers.
The first two creates a range.
The other two creates another range.
Print True or False whether the fifth number is between the two ranges at the same time or not
"""
first=int(input(("First number --> ")))
second=int(input(("Second number --> ")))
third=int(input(("Third number --> ")))
fourth=int(input(("Fourth number --> ")))
fifth=int(input(("Fifth number --> ")))
if first<fifth<second and third<fifth<fourth: print(True)
else: print(False)
