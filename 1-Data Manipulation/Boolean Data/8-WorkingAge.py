"""
Guillem Teixido
16/12/22

User inputs it's age
Print True or False whether the user is inside the Working Age or not
"""
WORK_AGE_MIN=16
WORK_AGE_MAX=65
age=int(input("Age --> "))
if WORK_AGE_MIN<=age<=WORK_AGE_MAX: print(True)
else: print(False)
