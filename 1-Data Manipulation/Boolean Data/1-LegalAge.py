"""
Made by Guillem Teixido
16/12/22

The user writes its age
Print True or False whether the age is Legal or not
"""
LEGAL_AGE=18
age = int(input("Your age --> "))
if age < LEGAL_AGE: print("You're underage")
else: print("You're an adult")
