"""
Guillem Teixido
DD/MM/YY

User inputs two integer numbers
Print True or False whether the first number can be divided by the second without any remainder.
"""
first_num=float(input("First number --> "))
second_num=float(input("Second number --> "))
if first_num%second_num==0: print(True)
else: print(False)
