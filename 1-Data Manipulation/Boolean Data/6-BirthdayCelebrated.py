"""
Guillem Teixido
16/12/22

User inputs it's birthday Month and Day.
Print True or False whether it's birthday has been celebrated this year

Modules used: datetime
"""
from datetime import datetime
DATE=datetime.now()
birthday_month=int(input("Birthday month number --> "))
birthday_day=int(input("Birthday day --> "))
if birthday_day<=DATE.day and birthday_month<DATE.month: print(True)
elif birthday_day<=DATE.day and birthday_month<=DATE.month: print("Today!")
else: print(False)
