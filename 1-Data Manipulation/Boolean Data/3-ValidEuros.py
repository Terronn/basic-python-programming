"""
Guillem Teixido
16/12/22

User inputs its euro banknote value
Print True or False whether the value is right or wrong
"""
NOTES=[5,10,20,50,100,200,500]
note=int(input("Your banknote value --> "))
if note in NOTES: print(True)
else: print(False)
