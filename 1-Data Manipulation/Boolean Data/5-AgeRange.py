"""
Guillem Teixido
16/12/22

User inputs its age.
Print True or False whether the age is between a determined range.
"""
MIN_AGE,MAX_AGE=10,20
age=int(input("Your age --> "))
if age>MIN_AGE and age<MAX_AGE: print(True)
else: print(False)
