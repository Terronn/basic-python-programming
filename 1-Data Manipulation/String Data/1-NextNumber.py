"""
Made by: Guillem Teixido
15/12/2022

User inputs a number
Print: "The next number is {number}"
"""
number = int(input("Number --> "))
print("The next number is {}".format(number+1))