"""
Made by: Guillem Teixido
15/12/2022

User inputs its name
Print: "Good morning {name}"
"""
name = input("Name --> ")
print("Good morning, {}".format(name))