"""
Made by: Guillem Teixido
15/12/2022

Read the height, width and depth measures of a room.
Print the volume of the room
"""
height = float(input("Room height (m) --> "))
width = float(input("Room width (m) --> "))
depth = float(input("Room depth (m) --> "))
print("The volume of the room is {}m³".format(round(height*width*depth,2)))