"""
Made by: Guillem Teixido
15/12/2022

Read the diameter of a circle area.
Print the total area.

*Used math library for PI*
"""
from math import pi
diameter = float(input("Diameter (cm) --> "))
print("The pizza is {} cm2".format(round(diameter*pi,1)))