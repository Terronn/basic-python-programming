"""
Made by: Guillem Teixido
15/12/2022

Read a price and a discounted price.
Then proceed to print the discount percentage applied.
"""
price = float(input("Price --> "))
discounted_price = float(input("Discounted Price --> "))
print("{}%".format(round((1-(discounted_price/price))*100,2)))
