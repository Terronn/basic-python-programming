"""
Made by: Guillem Teixido
15/12/2022

User inputs two numbers
Program prints which one is bigger, or if they are equal in that case
"""
numbers = input("Two numbers--> ").split()
num1, num2 = int(numbers[0]), int(numbers[1])
if num1 > num2: print(num1)
elif num2 > num1: print(num2)
else: print("Equal")