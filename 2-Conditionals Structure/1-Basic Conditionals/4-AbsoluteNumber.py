"""
Made by: Guillem Teixido
27/12/2022

User inputs a number whether is positive or negative
Program will output the absolute number of it (if its negative)
"""
number = int(input("Number --> "))
if number < 0: number = number*-1
print(number)