"""
Made by: Guillem Teixido
27/12/2022

User inputs a number from 1 to 3
Computer generates a random number from 1 to 3
Program will say if user or Computer wins the game

Libraries used --> Random
No Lists allowed
"""
import random
user = int(input("[user] - 1(Rock), 2(Paper), 3(Scissors) --> "))
computer = random.randint(1,3)

if computer == 1:print("Computer choosed Rock")
elif computer == 2:print("Computer choosed Paper")
else:print("Computer choosed Scissors")

if user in range(1,4):
    if user > computer or user - computer == 2:print("User Wins")
    elif computer > user or computer - user == 2:print("Computer Wins")
    else:print("Draw")
else:
    print("Bad option selected")