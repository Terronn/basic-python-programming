"""
Made by: Guillem Teixido
14/12/2022

User inputs a number from 1 to 3
User2 inputs a number from 1 to 3
Program will say if User1 or User2 wins the game
"""
user1 = int(input("[User1] - 1(Rock), 2(Paper), 3(Scissors) --> "))
user2 = int(input("[User1] - 1(Rock), 2(Paper), 3(Scissors) --> "))
if user1 in range(1,4) and user2 in range(1,4):
    if user1 > user2 or user1 - user2 == 2:print("User1 Wins")
    elif user2 > user1 or user2 - user1 == 2:print("User2 Wins")
    else:print("Draw")
else:
    print("Bad option/s selected")