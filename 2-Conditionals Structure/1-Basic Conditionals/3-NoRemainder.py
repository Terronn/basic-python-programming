"""
Made by: Guillem Teixido
15/12/2022

User inputs two numbers
Program prints True or False whether the remainder of the
division of both numbers is 0 or not.
"""
numbers = input("Two numbers--> ").split()
num1, num2 = int(numbers[0]), int(numbers[1])
if num2>num1: print("Error: Second number is bigger")
else:
    if num1%num2==0:print(True)
    else:print(False)