"""
Made by: Guillem Teixido
09/01/2023

User inputs a number (diameter of a circle)
User inputs two numbers (x and y of a square)
Print which shape is bigger.
"""
from math import pi

diameter = int(input("Circle Diameter --> "))
square = input("Square Axis (\"X Y\") --> ")
axis = square.split()

if diameter*pi > int(axis[0])*int(axis[1]): print("Circle is bigger")
else: print("Square is bigger")