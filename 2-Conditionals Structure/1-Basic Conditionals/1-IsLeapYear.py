"""
Made by: Guillem Teixido
15/12/2022

User inputs a year date {yyyy}
Program prints True or False whether it's a leap year or not
"""
year = int(input("Year --> "))
if year < 1582:
    print("Year too early (No leap years before 1582)")
else:
    if year % 4 != 0: result=False
    else:
        if year % 400 == 0: result=True
        elif year % 100 != 0: result=True
        else: result=False
    print(result)