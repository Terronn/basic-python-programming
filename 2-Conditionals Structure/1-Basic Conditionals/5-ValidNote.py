"""
Made by: Guillem Teixido
09/01/2023

User inputs a number of a money note
Print True or False whether the note value is correct or not

Without using lists.
"""
note = int(input("Note Value --> "))
if note != 5 and note != 10 and note != 20 and note != 50 and note != 100: print(False)
else: print(True)