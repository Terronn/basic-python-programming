"""
No lists allowed
"""
from datetime import datetime
loop = True
while loop == True:
    secs = int(input("How many seconds you want to advance --> "))
    if secs > 60:
            print("Invalid Number (Max=60)")
    else: loop = False
time = datetime.now()
currenttime = time.strftime("%H:%M:%S")
current = currenttime.split(":")
datehour = int(current[0])
datemins = int(current[1])
datesecs = int(current[2])
if (secs + datesecs) > 60:
    datesecs = (secs + datesecs) - 60
    datemins = datemins+1
    if datemins == 60:
        datemins = 00
        datehour += 1
        if datehour == 24: datehour = 0
else: datesecs = datesecs + secs
if datehour < 10: datehour = "0{}".format(datehour)
if datemins < 10: datemins = "0{}".format(datemins)
if datesecs < 10: datesecs = "0{}".format(datesecs)
print("Current Time --> {}".format(currenttime))
print("Advanced Time --> {}:{}:{}".format(datehour,datemins,datesecs))
