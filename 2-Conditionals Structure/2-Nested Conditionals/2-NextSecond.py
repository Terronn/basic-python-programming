"""
No lists allowed
"""
hour = input("Time (hh:mm:ss) --> ")
hour = hour.split(":")
hour0 = int(hour[0])
hour1 = int(hour[1])
hour2 = int(hour[2])
if (hour0 >= 24) or (hour1 >= 60) or (hour2 >= 60):
    print("Incorrect time format (hh:mm:ss)")
    exit()
if hour2 < 59: hour2 = hour2+1
else:
    hour2 = 00
    hour1 = hour1+1
    if hour1 == 60:
        hour1 = 00
        hour0 = hour0 + 1
        if hour0 == 24: hour0 = 00
if hour0 < 10: hour0 = "0{}".format(hour2)
if hour1 < 10: hour1 = "0{}".format(hour1)
if hour2 < 10: hour2 = "0{}".format(hour2)
print("{}:{}:{}".format(hour0,hour1,hour2))
